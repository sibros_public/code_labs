## C Unit Testing Code Labs

Test your C unit testing skills with `Unity` and `CMock`!

### Setup

1. Install Ceedling with `$ gem install ceedling`, requires Ruby and GCC
2. Make new Ceedling project with `$ ceedling new [NAME]`, this will create a new project folder with `src` and `test` directories already set up.
3. In `project.yml` under the `:cmock:` heading, ensure that `:mock_prefix:` is set to correct mocked header prefix, also feel free to add more `:plugins:` depending on how you wish to mock out your methods.
4. Source code goes in `src/`, test code goes in `test/`
5. Run tests with `$ ceedling test:all` or `$ ceedling` within project folder

## Instructions and Tips
Check out our [Confluence page](https://sibros.atlassian.net/wiki/spaces/SP/pages/101515515/Unit+Testing+for+C#Basics-of-Unity-%26-CMock) on unit testing for C, and read the documentation for [Unity](http://www.throwtheswitch.org/unity) and [CMock](http://www.throwtheswitch.org/cmock).