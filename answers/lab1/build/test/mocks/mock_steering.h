/* AUTOGENERATED FILE. DO NOT EDIT. */
#ifndef _MOCK_STEERING_H
#define _MOCK_STEERING_H

#include "unity.h"
#include "steering.h"

/* Ignore the following warnings, since we are copying code */
#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic push
#endif
#if !defined(__clang__)
#pragma GCC diagnostic ignored "-Wpragmas"
#endif
#pragma GCC diagnostic ignored "-Wunknown-pragmas"
#pragma GCC diagnostic ignored "-Wduplicate-decl-specifier"
#endif

void mock_steering_Init(void);
void mock_steering_Destroy(void);
void mock_steering_Verify(void);




#define steering__steer_left_Ignore() steering__steer_left_CMockIgnore()
void steering__steer_left_CMockIgnore(void);
#define steering__steer_left_StopIgnore() steering__steer_left_CMockStopIgnore()
void steering__steer_left_CMockStopIgnore(void);
#define steering__steer_left_Expect() steering__steer_left_CMockExpect(__LINE__)
void steering__steer_left_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_steering__steer_left_CALLBACK)(int cmock_num_calls);
void steering__steer_left_AddCallback(CMOCK_steering__steer_left_CALLBACK Callback);
void steering__steer_left_Stub(CMOCK_steering__steer_left_CALLBACK Callback);
#define steering__steer_left_StubWithCallback steering__steer_left_Stub
#define steering__steer_right_Ignore() steering__steer_right_CMockIgnore()
void steering__steer_right_CMockIgnore(void);
#define steering__steer_right_StopIgnore() steering__steer_right_CMockStopIgnore()
void steering__steer_right_CMockStopIgnore(void);
#define steering__steer_right_Expect() steering__steer_right_CMockExpect(__LINE__)
void steering__steer_right_CMockExpect(UNITY_LINE_TYPE cmock_line);
typedef void (* CMOCK_steering__steer_right_CALLBACK)(int cmock_num_calls);
void steering__steer_right_AddCallback(CMOCK_steering__steer_right_CALLBACK Callback);
void steering__steer_right_Stub(CMOCK_steering__steer_right_CALLBACK Callback);
#define steering__steer_right_StubWithCallback steering__steer_right_Stub

#if defined(__GNUC__) && !defined(__ICC) && !defined(__TMS470__)
#if __GNUC__ > 4 || (__GNUC__ == 4 && (__GNUC_MINOR__ > 6 || (__GNUC_MINOR__ == 6 && __GNUC_PATCHLEVEL__ > 0)))
#pragma GCC diagnostic pop
#endif
#endif

#endif
