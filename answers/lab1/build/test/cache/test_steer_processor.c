#include "src/steer_processor.h"
#include "mock_steering.h"
#include "/Users/jonathan/miniconda3/envs/platform/lib/ruby/gems/2.6.0/gems/ceedling-0.31.0/vendor/unity/src/unity.h"












void setUp(void) {}



void tearDown(void) {}















void test_steer_processor__move_left(void) {

  steering__steer_left_CMockExpect(75);

  steer_processor__steer(51U, 49U);

}



void test_steer_processor__move_right(void) {

  steering__steer_right_CMockExpect(80);

  steer_processor__steer(49U, 51U);

}



void test_steer_processor__both_sensors_less_than_threshold_and_equal(void) { steer_processor__steer(49U, 49U); }



void test_steer_processor__both_sensors_more_than_threshold(void) { steer_processor__steer(51U, 51U); }



void test_steer_processor(void) {

  steering__steer_right_CMockExpect(89);

  steer_processor__steer(10U, 20U);



  steering__steer_left_CMockExpect(92);

  steer_processor__steer(20U, 10U);

}
