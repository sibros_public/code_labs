/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/**
 * @file queue_lab.h
 * FIFO circular uint8_t queue of size 100, statically defined and fixed at compile time
 *
 * Thread Safety Assessment: N/A
 */
#ifndef SIBROS__QUEUE_LAB_H
#define SIBROS__QUEUE_LAB_H
#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

/* External Includes */

/* Module Includes */

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

#define QUEUE_LAB__QUEUE_SIZE (100)

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

typedef struct {
  uint8_t queue_memory[QUEUE_LAB__QUEUE_SIZE];

  uint8_t front;
  uint8_t rear;

  size_t current_size;
  size_t max_size;
} queue_s;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

bool queue__init(queue_s *queue);

bool queue__push(queue_s *queue, uint8_t push_value);

bool queue__pop(queue_s *queue, uint8_t *pop_value);

size_t queue__get_item_count(const queue_s *queue);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* #ifdef SIBROS__QUEUE_LAB_H */
