/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "queue_lab.h"

/* Standard Includes */

/* External Includes */

/* Private Module Includes */

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

bool queue__init(queue_s *queue) {
  bool return_value = false;

  if (NULL != queue) {
    queue->front = 0U;
    queue->rear = 0U;
    queue->current_size = 0U;
    queue->max_size = QUEUE_LAB__QUEUE_SIZE;
    memset(queue->queue_memory, 0, queue->max_size);

    return_value = true;
  }

  return return_value;
}

size_t queue__get_item_count(const queue_s *queue) {
  size_t return_value = 0U;

  if (NULL != queue) {
    return_value = queue->current_size;
  }

  return return_value;
}

bool queue__push(queue_s *queue, uint8_t push_value) {
  bool return_value = false;

  if (NULL != queue) {
    if (queue->max_size == queue->current_size) {
      return_value = false;
    } else if (queue->max_size > queue->current_size) {
      queue->queue_memory[queue->rear] = push_value;
      queue->rear = (queue->rear + 1U) % (uint8_t)(queue->max_size);
      queue->current_size = queue->current_size + (size_t)1;
      return_value = true;
    } else {
      return_value = false;
    }
  }

  return return_value;
}

bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  bool return_value = false;

  if (NULL != pop_value) {
    if (NULL != queue) {
      if (0U == queue->current_size) {
        *pop_value = 0U;
        return_value = false;
      } else if (queue->max_size >= queue->current_size) {
        *pop_value = queue->queue_memory[queue->front];
        queue->front = (queue->front + 1U) % (uint8_t)(queue->max_size);
        queue->current_size = queue->current_size - (size_t)1;
        return_value = true;
      } else {
        *pop_value = 0U;
        return_value = false;
      }
    } else {
      *pop_value = 0U;
      return_value = false;
    }
  }

  return return_value;
}
