/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */
#include <stdbool.h>
#include "unity.h"

/* Mock Includes */

/* External Includes */

/* Module Includes */
#include "queue_lab.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

static queue_s queue;

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                     T E S T   S E T U P   &   T E A R D O W N
 *
 **********************************************************************************************************************/

void setUp(void) { TEST_ASSERT_TRUE(queue__init(&queue)); }

void tearDown(void) {
  queue.front = 0U;
  queue.rear = 0U;
  queue.current_size = 0U;
  queue.max_size = 0U;
  memset(queue.queue_memory, 0, QUEUE_LAB__QUEUE_SIZE);
}

/***********************************************************************************************************************
 *
 *                                                     T E S T S
 *
 **********************************************************************************************************************/

/* queue__init tests */

void test_queue_lab__init_null_queue_pointer(void) {
  void *null_ptr = NULL;
  TEST_ASSERT_FALSE(queue__init(null_ptr));
}

void test_queue_lab__init_empty_queue(void) {
  const queue_s expected_queue = {.front = 0U, .rear = 0U, .current_size = 0U};
  TEST_ASSERT_EQUAL(expected_queue.front, queue.front);
  TEST_ASSERT_EQUAL(expected_queue.rear, queue.rear);
  TEST_ASSERT_EQUAL(expected_queue.current_size, queue.current_size);
}

/* queue__get_item_count tests */

void test_queue_lab__get_item_count_of_null_pointer(void) {
  void *null_ptr = NULL;
  size_t test_result = queue__get_item_count(null_ptr);
  const size_t expected_result = 0U;
  TEST_ASSERT_EQUAL(expected_result, test_result);
}

void test_queue_lab__get_item_count_of_empty_queue(void) {
  size_t queue_get_item_count_test_result = queue__get_item_count(&queue);
  const size_t expected_result = 0U;
  TEST_ASSERT_EQUAL(expected_result, queue_get_item_count_test_result);
}

void test_queue_lab__get_item_count_of_full_queue(void) {
  const uint8_t push_value = 1U; /* Some random value for testing purposes */
  for (size_t current_size = 0U; queue.max_size > current_size; ++current_size) {
    TEST_ASSERT_TRUE(queue__push(&queue, push_value));
  }

  size_t queue_get_item_count_test_result = queue__get_item_count(&queue);
  const size_t expected_result = QUEUE_LAB__QUEUE_SIZE;
  TEST_ASSERT_EQUAL(expected_result, queue_get_item_count_test_result);
}

/* queue__push tests */

void test_queue_lab__push_null_pointer(void) {
  void *null_ptr = NULL;
  uint8_t test_value = 0U; /* Some random value for testing purposes */
  bool test_result = queue__push(null_ptr, test_value);
  TEST_ASSERT_FALSE(test_result);
}

void test_queue_lab__push_empty_queue(void) {
  uint8_t push_value = 5U;
  TEST_ASSERT_TRUE(queue__push(&queue, push_value));

  queue_s expected_queue = {.front = 0U, .rear = 1U, .current_size = 1U};
  TEST_ASSERT_EQUAL(expected_queue.front, queue.front);
  TEST_ASSERT_EQUAL(expected_queue.rear, queue.rear);
  TEST_ASSERT_EQUAL(expected_queue.current_size, queue.current_size);
}

void test_queue_lab__push_full_queue(void) {
  const uint8_t push_value = 1U; /* Some random value for testing purposes */
  for (size_t current_size = 0U; queue.max_size > current_size; ++current_size) {
    TEST_ASSERT_TRUE(queue__push(&queue, push_value));
  }

  TEST_ASSERT_FALSE(queue__push(&queue, push_value));

  queue_s expected_queue = {.front = 0U, .rear = 0U, .current_size = 100U};
  TEST_ASSERT_EQUAL(expected_queue.front, queue.front);
  TEST_ASSERT_EQUAL(expected_queue.rear, queue.rear);
  TEST_ASSERT_EQUAL(expected_queue.current_size, queue.current_size);
}

void test_queue_lab__queue_wrap_around(void) {
  const uint8_t push_value = 1U; /* Some random value */
  for (size_t current_size = 0U; queue.max_size > current_size; ++current_size) {
    TEST_ASSERT_TRUE(queue__push(&queue, push_value));
  }

  for (size_t current_size = 0U; queue.max_size > current_size; ++current_size) {
    uint8_t pop_value = 0U;
    TEST_ASSERT_TRUE(queue__pop(&queue, &pop_value));
    TEST_ASSERT_EQUAL(push_value, pop_value);
  }

  const uint32_t expected_front = 0U;
  const uint32_t expected_rear = 0U;
  const size_t expected_size = 0U;

  TEST_ASSERT_EQUAL(expected_front, queue.front);
  TEST_ASSERT_EQUAL(expected_rear, queue.rear);
  TEST_ASSERT_EQUAL(expected_size, queue.current_size);
}

void test_queue_lab__push_invalid_size(void) {
  queue.current_size = 101U;
  const uint8_t push_value = 1U; /* Some random value */

  TEST_ASSERT_FALSE(queue__push(&queue, push_value));
}

/* queue__pop tests */

void test_queue_lab__pop_null_pointer(void) {
  void *null_ptr = NULL;
  uint8_t test_value = 255U; /* Placeholder value, will be overwritten by pop */
  TEST_ASSERT_FALSE(queue__pop(null_ptr, &test_value));
  TEST_ASSERT_EQUAL(0U, test_value);
}

void test_queue_lab__pop_to_null_pointer(void) {
  uint8_t *null_pop_value = NULL;

  TEST_ASSERT_FALSE(queue__pop(&queue, null_pop_value));
}

void test_queue_lab__pop_empty_queue(void) {
  uint8_t pop_value;
  TEST_ASSERT_FALSE(queue__pop(&queue, &pop_value));
  TEST_ASSERT_EQUAL(0U, pop_value);

  queue_s expected_queue = {.front = 0U, .rear = 0U, .current_size = 0U};
  TEST_ASSERT_EQUAL(expected_queue.front, queue.front);
  TEST_ASSERT_EQUAL(expected_queue.rear, queue.rear);
  TEST_ASSERT_EQUAL(expected_queue.current_size, queue.current_size);
}

void test_queue_lab__pop_full_queue(void) {
  /* values starting from 0 and increasing by 1 up to 100 get pushed into queue, so when value gets popped off full
   * queue, it should be 0 */
  uint8_t push_value = 0U;
  uint8_t initial_push_value = push_value;
  for (size_t current_size = 0U; queue.max_size > current_size; ++current_size) {
    TEST_ASSERT_TRUE(queue__push(&queue, push_value));
    ++push_value;
  }

  uint8_t pop_value;
  TEST_ASSERT_TRUE(queue__pop(&queue, &pop_value));
  TEST_ASSERT_EQUAL(initial_push_value, pop_value);

  queue_s expected_queue = {.front = 1U, .rear = 0U, .current_size = 99U};
  TEST_ASSERT_EQUAL(expected_queue.front, queue.front);
  TEST_ASSERT_EQUAL(expected_queue.rear, queue.rear);
  TEST_ASSERT_EQUAL(expected_queue.current_size, queue.current_size);
}

void test_queue_lab__pop_invalid_size(void) {
  queue.current_size = 101U;
  uint8_t pop_value = 1; /* Random non-zero value */

  TEST_ASSERT_FALSE(queue__pop(&queue, &pop_value));
  TEST_ASSERT_EQUAL(0U, pop_value);
}
