/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "queue_lab2.h"

/* Standard Includes */

/* External Includes */

/* Private Module Includes */

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

bool queue__init(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {
  bool return_value = false;

  if ((NULL != queue) && (NULL != static_memory_for_queue)) {
    queue->front = 0U;
    queue->rear = 0U;
    queue->size = 0U;

    queue->static_memory_for_queue = static_memory_for_queue;
    queue->static_memory_size_in_bytes = static_memory_size_in_bytes;

    memset(queue->static_memory_for_queue, 0, queue->static_memory_size_in_bytes);

    return_value = true;
  }

  return return_value;
}

size_t queue__get_item_count(const queue_s *queue) {
  size_t return_value = 0U;

  if (NULL != queue) {
    return_value = queue->size;
  }

  return return_value;
}

/* @returns false if the queue is full */
bool queue__push(queue_s *queue, uint8_t push_value) {
  bool return_value = false;

  if (NULL != queue) {
    if (queue->static_memory_size_in_bytes <= queue->size) {
      return_value = false;
    } else {
      *(uint8_t *)((queue->static_memory_for_queue) + queue->rear) = push_value;
      queue->rear = (queue->rear + 1U) % (uint32_t)(queue->static_memory_size_in_bytes);
      queue->size = queue->size + 1U;
      return_value = true;
    }
  }

  return return_value;
}

/* @returns false if the queue was empty */
bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  bool return_value = false;

  if (NULL != pop_value) {
    if ((NULL != queue) && (0U < queue->size)) {
      *pop_value = *(uint8_t *)(queue->static_memory_for_queue + queue->front);
      queue->front = (queue->front + 1U) % (uint32_t)(queue->static_memory_size_in_bytes);
      queue->size = queue->size - 1U;
      return_value = true;
    }

    if (!return_value) {
      *pop_value = 0U;
    }
  }

  return return_value;
}
