/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/**
 * @file queue_lab2.h
 * FIFO circular queue with user defined static memory
 *
 * Thread Safety Assessment: N/A
 */
#ifndef SIBROS__QUEUE_LAB2_H
#define SIBROS__QUEUE_LAB2_H
#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

/* External Includes */

/* Module Includes */

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/* In this code lab, the queue memory is statically defined
 * by the user and provided to you upon queue__init()
 */
typedef struct {
  void *static_memory_for_queue;
  size_t static_memory_size_in_bytes;

  uint32_t front;
  uint32_t rear;
  size_t size;
  /* TODO: Add more members as needed */
} queue_s;

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/* Initialize the queue with user provided static memory
 * @param static_memory_for_queue This memory pointer should not go out of scope
 */
bool queue__init(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes);

/* @returns false if the queue is full */
bool queue__push(queue_s *queue, uint8_t push_value);

/* @returns false if the queue was empty */
bool queue__pop(queue_s *queue, uint8_t *pop_value);

size_t queue__get_item_count(const queue_s *queue);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* #ifdef SIBROS__QUEUE_LAB2_H */
