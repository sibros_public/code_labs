/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/**
 * @file message_processor.h
 * Reads three messages, and checks the third message for a '$' or '#' at the first index, and if so, write a '^' or '%'
 * at the second index respectively
 *
 * Thread Safety Assessment: N/A
 */
#ifndef SIBROS__MESSAGE_PROCESSOR_H
#define SIBROS__MESSAGE_PROCESSOR_H
#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */
#include <stdbool.h>
#include <stddef.h>
#include <string.h>

/* External Includes */

/* Module Includes */
#include "message.h"
// #include "sl_common.h"
// #include "sl_unit_test_facilitator.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                     F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

bool message_processor(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* #ifdef SIBROS__MESSAGE_PROCESSOR_H */
