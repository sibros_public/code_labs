/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Main Module Header */
#include "message_processor.h"

/* Standard Includes */

/* External Includes */

/* Private Module Includes */
#include "message_processor_private.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

static bool check_third_message(message_s *third_message) {
  bool symbol_found = false;

  if (NULL != third_message) {
    if ('$' == third_message->data[0]) {
      third_message->data[1] = '^';
      symbol_found = true;
    } else if ('#' == third_message->data[0]) {
      third_message->data[1] = '%';
      symbol_found = true;
    } else {
      NOOP("Symbol not found");
      symbol_found = false;
    }
  }

  return symbol_found;
}

/***********************************************************************************************************************
 *
 *                                          P U B L I C   F U N C T I O N S
 *
 **********************************************************************************************************************/

bool message_processor(void) {
  bool symbol_found = false;
  message_s message = {.data = {0}};

  const static size_t max_messages_to_process = 3U;
  for (size_t message_count = 0U; max_messages_to_process > message_count; ++message_count) {
    if (!message__read(&message)) {
      break;
    } else if (message_count < 2U) {
      NOOP("Wait for last message");
    } else {
      symbol_found = check_third_message(&message);
    }
  }

  return symbol_found;
}
