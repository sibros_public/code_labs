/***********************************************************************************************************************
 * SIBROS TECHNOLOGIES, INC. CONFIDENTIAL
 * Copyright (c) 2018 - 2020 Sibros Technologies, Inc.
 * All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Sibros Technologies, Inc. and its suppliers,
 * if any. The intellectual and technical concepts contained herein are proprietary to Sibros Technologies, Inc. and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or
 * copyright law. Dissemination of this information or reproduction of this material is strictly forbidden unless prior
 * written permission is obtained from Sibros Technologies, Inc.
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  I N C L U D E S
 *
 **********************************************************************************************************************/
/* Standard Includes */
#include <string.h>
#include "unity.h"

/* Mock Includes */
#include "mock_message.h"

/* External Includes */

/* Module Includes */
#include "message_processor.h"
#include "message_processor_private.h"

/***********************************************************************************************************************
 *
 *                                                   D E F I N E S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                                  T Y P E D E F S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                             P R I V A T E   F U N C T I O N   D E C L A R A T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                  P R I V A T E   D A T A   D E F I N I T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                         P R I V A T E   F U N C T I O N S
 *
 **********************************************************************************************************************/

/***********************************************************************************************************************
 *
 *                                     T E S T   S E T U P   &   T E A R D O W N
 *
 **********************************************************************************************************************/

void setUp(void) {}

void tearDown(void) {}

/***********************************************************************************************************************
 *
 *                                                     T E S T S
 *
 **********************************************************************************************************************/

void test_message_processor__message_processor_no_messages(void) {
  message__read_ExpectAndReturn(NULL, false);
  message__read_IgnoreArg_message_to_read();

  TEST_ASSERT_FALSE(message_processor());
}

void test_message_processor__message_processor_third_message_no_symbol(void) {
  message_s message_without_symbol;
  memset(&message_without_symbol, 0, sizeof(message_without_symbol));

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  TEST_ASSERT_FALSE(message_processor());
}

void test_message_processor__message_processor_third_message_dollar_sign(void) {
  message_s message_with_symbol;
  memset(&message_with_symbol, 0, sizeof(message_with_symbol));
  message_with_symbol.data[0] = '$';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message_with_symbol);

  TEST_ASSERT_TRUE(message_processor());
}

void test_message_processor__message_processor_third_message_hash_tag(void) {
  message_s message_with_symbol;
  sl_utils__memset_zero(&message_with_symbol, sizeof(message_with_symbol));
  message_with_symbol.data[0] = '#';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message_with_symbol);

  TEST_ASSERT_TRUE(message_processor());
}

void test_message_processor__check_third_message_null_parameter(void) {
  message_s *null_ptr = NULL;
  bool test_result = check_third_message(null_ptr);
  TEST_ASSERT_EQUAL(NULL, null_ptr);
  TEST_ASSERT_FALSE(test_result);
}

void test_message_processor__check_third_message_no_symbols(void) {
  message_s original_message_without_symbol;
  memset(&original_message_without_symbol, 0, sizeof(original_message_without_symbol));
  message_s message_without_symbol;
  memset(&message_without_symbol, 0, sizeof(message_without_symbol));

  bool test_result = check_third_message(&message_without_symbol);
  TEST_ASSERT_FALSE(test_result);
  TEST_ASSERT_EQUAL_MEMORY(&original_message_without_symbol, &message_without_symbol,
                           sizeof(original_message_without_symbol));
}

void test_message_processor__check_third_message_dollar_sign(void) {
  message_s message_with_dollar_sign;
  memset(&message_with_dollar_sign, 0, sizeof(message_with_dollar_sign));
  message_with_dollar_sign.data[0] = '$';

  message_s expected_message = message_with_dollar_sign;
  expected_message.data[1] = '^';

  bool test_result = check_third_message(&message_with_dollar_sign);
  TEST_ASSERT_TRUE(test_result);
  TEST_ASSERT_EQUAL_MEMORY(&expected_message, &message_with_dollar_sign, sizeof(expected_message));
}

void test_message_processor__check_third_message_hash_tag(void) {
  message_s message_with_hash_tag;
  memset(&message_with_hash_tag, 0, sizeof(message_with_hash_tag));
  message_with_hash_tag.data[0] = '#';

  message_s expected_message = message_with_hash_tag;
  expected_message.data[1] = '%';

  bool test_result = check_third_message(&message_with_hash_tag);
  TEST_ASSERT_TRUE(test_result);
  TEST_ASSERT_EQUAL_MEMORY(&expected_message, &message_with_hash_tag, sizeof(expected_message));
}
