#include <stdbool.h>
#include <stddef.h>
#include <string.h>

#include "message_processor.h"
#include "sl_common.h"

bool message_processor(void) {
  bool symbol_found = false;
  message_s message;
  memset(&message, 0, sizeof(message));

  const static size_t max_messages_to_process = 3;
  for (size_t message_count = 0; message_count < max_messages_to_process; message_count++) {
    if (!message__read(&message)) {
      break;
    } else if (message_count < 2) {
      NOOP("Wait for last message");
    } else {
      if (message.data[0] == '$') {
        message.data[1] = '^';
        symbol_found = true;
      } else if (message.data[0] == '#') {
        message.data[1] = '%';
        symbol_found = true;
      } else {
        NOOP("Symbol not found");
      }
    }
  }

  return symbol_found;
}